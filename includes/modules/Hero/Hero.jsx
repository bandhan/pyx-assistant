// External Dependencies
import React, { Component } from 'react';

class PyxHero extends Component {

  static slug = 'pyx_hero';

  // Grid configuration feature added
	grids() {
		const props = this.props;
		let grid_props = {
      grid_2: [
        props['image_02'] ? "url("+props['image_02']+")" : props['color_02'],
        props['react_grid_2'] === 'on' ? '1 / span 2' : '',
      ],
			grid_3: [
        props['image_03'] ? "url("+props['image_03']+")" : props['color_03'],
        props['react_grid_3'] === 'on' ? '1 / span 2' : '',
      ],
			grid_4: [
        props['image_04'] ? "url("+props['image_04']+")" : props['color_04'],
        props['react_grid_4'] === 'on' ? '1 / span 2' : '',
      ],
			grid_5: [
        props['image_05'] ? "url("+props['image_05']+")" : props['color_05'],
        props['react_grid_5'] === 'on' ? '1 / span 2' : '',
      ],
    };

    const grid_keys = Object.keys( grid_props );

    return (
        <div className="nested">
          {grid_keys.map( ( grid_prop, index ) => {
              return (
                grid_props[grid_prop][0] &&
                <div key={ index } className="pyx-nested-box" style={{background: grid_props[grid_prop][0], gridColumn: grid_props[grid_prop][1]}}></div>
              )
          })}
        </div>
      );
	}

  _renderButton() {
    let buttonStyle = {};
    const props = this.props;
    const utils = window.ET_Builder.API.Utils;
    const buttonTarget = 'on' === props.url_new_window ? '_blank' : '';
    const buttonClassName = {
      'et_pb_button btnReverse et_pb_button_0 btnInline et_pb_bg_layout_light': true,
    };

    if( props.hero_type === 'on' ) {
      buttonStyle = {fontSize: '18px', color: '#009dc9', paddingLeft: '20px', paddingRight: '20px'};
    } else {
      buttonStyle = {fontSize: '18px', backgroundColor: '#fff', borderColor: '#fff', color: '#009dc9', paddingLeft: '20px', paddingRight: '20px'};
    }

    if (!props.button_text || !props.button_url) {
      return '';
    }

    return (
      <div className='et_pb_button_wrapper et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_left et_pb_module'
        style={{ marginTop: '40px' }}>
        <a
          className={utils.classnames(buttonClassName)}
          href={props.button_url}
          style={buttonStyle}
          target={buttonTarget}
        >
          {props.button_text}
        </a>
      </div>
    );
  }

  render() {
    const Content = this.props.content;

    return (
      <>
        <div className={this.props.hero_type !== 'on' ? "wrapper no-home" : "wrapper"}>
          <div className="box1">
          {this.props.hero_type !== 'on' &&
            <div className="box1-inner">
              <h1>{this.props.title}</h1>
              <Content />
              {this._renderButton()}
            </div>
          }
          {this.props.hero_type === 'on' &&
          <>
            <h1>{this.props.title}</h1>
            <Content />
            {this._renderButton()}
          </>
          }
          </div>

          {this.grids()}
        </div>

        {this.props.hero_type === 'on' &&
        <div className="wrapper2">
          <div className="box6">
          </div>
          <div className="box7">
          </div>
          <div className="box8">
          </div>
          <div className="box9">
          </div>
          <div className="box10">
          </div>
        </div>
        }
      </>
    );
  }
}

export default PyxHero;
