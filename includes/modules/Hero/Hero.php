<?php

class PyxHero extends ET_Builder_Module {

	public $slug       = 'pyx_hero';
	public $vb_support = 'on';

	protected $module_credits = array(
		'author'     => 'TreeTreeAgency',
		'author_uri' => 'https://treetreeagency.com/',
	);

	// Grid configuration feature added
	private function grids() {
		$props = $this->props;
		$grid_props = array(
			'grid_2' => array(
				$props['image_02'] ? "url(".$props['image_02'].")" : $props['color_02'],
				$props['react_grid_2'] == 'on' ? '; grid-column: 1 / span 2' : '',
			),
			'grid_3' => array(
				$props['image_03'] ? "url(".$props['image_03'].")" : $props['color_03'],
				$props['react_grid_3'] == 'on' ? '; grid-column: 1 / span 2' : '',
			),
			'grid_4' => array(
				$props['image_04'] ? "url(".$props['image_04'].")" : $props['color_04'],
				$props['react_grid_4'] == 'on' ? '; grid-column: 1 / span 2' : '',
			),
			'grid_5' => array(
				$props['image_05'] ? "url(".$props['image_05'].")" : $props['color_05'],
				$props['react_grid_5'] == 'on' ? '; grid-column: 1 / span 2' : '',
			),
		);

		if( ! empty( array_filter( $grid_props ) ) ) : ?>
			<div class="nested">
			<?php
			foreach( $grid_props as $grid_prop ) :
				if( ! empty( $grid_prop[0] ) ) :
			?>
				<div class="pyx-nested-box" style="background: <?php echo $grid_prop[0] . $grid_prop[1]; ?>"></div>
			<?php
				endif;
			endforeach;
			?>
			</div>
			<?php
		endif;
	}

	// Added button styles
	private function button_style( $style ) {
		$homepage = array (
			"on"  	=> "font-size: 18px; color: #009dc9; padding-left: 20px !important; padding-right: 20px !important;",
		);

		if( empty( $style ) || 'off' == $style ) {
			return "font-size: 18px; background-color: #fff; border-color: #fff; color: #009dc9!important; padding-left: 20px !important; padding-right: 20px !important;";
		}

		return $homepage[$style];
	}

	// Add new dom based on switch field
	private function new_dom( $cond, $dm ) {
		if( $cond == 'off' || $cond == '' ) {
			return $dm;
		}
	}

	// Divi module initialization
	public function init() {
		$this->name = esc_html__( 'Hero', 'pyx-ast' );
		$this->fullwidth       = true;
		$this->use_raw_content = true;

		// Toggle settings
		$this->settings_modal_toggles  = array(
			'general'  => array(
				'toggles' => array(
					'grid_01'  => esc_html__( 'Grid #1', 'pyx-ast' ),
					'grid_02'  => esc_html__( 'Grid #2', 'pyx-ast' ),
					'grid_03'  => esc_html__( 'Grid #3', 'pyx-ast' ),
					'grid_04'  => esc_html__( 'Grid #4', 'pyx-ast' ),
					'grid_05'  => esc_html__( 'Grid #5', 'pyx-ast' ),
				),
			),
		);

	}

	// Neccessary fields for the module
	public function get_fields() {
        return array(
			'hero_type'	=> array(
				'label'           => esc_html__('Select hero for homepage', 'pyx-ast'),
				'type'            => 'yes_no_button',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Two types of hero sections can be used with this module.', 'pyx-ast'),
				'toggle_slug'     => 'grid_01',
				'options'		  => array(
					'off'	=> 'Off',
					'on'	=> 'On',
				)
			),
			'react_grid_2'	=> array(
				'label'           => esc_html__('Rectangle', 'pyx-ast'),
				'type'            => 'yes_no_button',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Switch on to make the square a rectangle.', 'pyx-ast'),
				'toggle_slug'     => 'grid_02',
				'options'		  => array(
					'off'	=> 'Off',
					'on'	=> 'On',
				)
			),
			'react_grid_3'	=> array(
				'label'           => esc_html__('Rectangle', 'pyx-ast'),
				'type'            => 'yes_no_button',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Switch on to make the square a rectangle.', 'pyx-ast'),
				'toggle_slug'     => 'grid_03',
				'options'		  => array(
					'off'	=> 'Off',
					'on'	=> 'On',
				)
			),
			'react_grid_4'	=> array(
				'label'           => esc_html__('Rectangle', 'pyx-ast'),
				'type'            => 'yes_no_button',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Switch on to make the square a rectangle.', 'pyx-ast'),
				'toggle_slug'     => 'grid_04',
				'options'		  => array(
					'off'	=> 'Off',
					'on'	=> 'On',
				)
			),
			'react_grid_5'	=> array(
				'label'           => esc_html__('Rectangle', 'pyx-ast'),
				'type'            => 'yes_no_button',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Switch on to make the square a rectangle.', 'pyx-ast'),
				'toggle_slug'     => 'grid_05',
				'options'		  => array(
					'off'	=> 'Off',
					'on'	=> 'On',
				)
			),
			'title'	=> array(
				'label'           => esc_html__('Title', 'pyx-ast'),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Hero title', 'pyx-ast'),
				'toggle_slug'     => 'grid_01',
			),
			'content'	=> array(
				'label'           => esc_html__('Hero content', 'pyx-ast'),
				'type'            => 'tiny_mce',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Content entered here will appear inside the hero.', 'pyx-ast'),
				'toggle_slug'     => 'grid_01',
			),
			'button_text' => array(
				'label'           => esc_html__('Button Text', 'pyx-ast'),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Input your desired button text, or leave blank for no button.', 'pyx-ast'),
				'toggle_slug'     => 'grid_01',
			),
			'button_url' => array(
				'label'           => esc_html__('Button URL', 'pyx-ast'),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__('Input URL for your button.', 'pyx-ast'),
				'toggle_slug'     => 'grid_01',
			),
			'button_url_new_window' => array(
				'default'         => 'off',
				'default_on_front' => true,
				'label'           => esc_html__('Url Opens', 'pyx-ast'),
				'type'            => 'select',
				'option_category' => 'configuration',
				'options'         => array(
					'off' => esc_html__('In The Same Window', 'pyx-ast'),
					'on'  => esc_html__('In The New Tab', 'pyx-ast'),
				),
				'toggle_slug'     => 'grid_01',
				'description'     => esc_html__('Choose whether your link opens in a new window or not', 'pyx-ast'),
			),
			'color_02' => array(
				'label'           => esc_html__( 'Color', 'dicm-divi-custom-modules' ),
				'type'            => 'color',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as title.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_02',
			),
			'image_02' => array(
				'label'           => esc_html__( 'Image', 'dicm-divi-custom-modules' ),
				'type'            => 'upload',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as subtitle.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_02',
			),
			'color_03' => array(
				'label'           => esc_html__( 'Color', 'dicm-divi-custom-modules' ),
				'type'            => 'color',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as title.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_03',
			),
			'image_03' => array(
				'label'           => esc_html__( 'Image', 'dicm-divi-custom-modules' ),
				'type'            => 'upload',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as subtitle.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_03',
			),
			'color_04' => array(
				'label'           => esc_html__( 'Color', 'dicm-divi-custom-modules' ),
				'type'            => 'color',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as title.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_04',
			),
			'image_04' => array(
				'label'           => esc_html__( 'Image', 'dicm-divi-custom-modules' ),
				'type'            => 'upload',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as subtitle.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_04',
			),
			'color_05' => array(
				'label'           => esc_html__( 'Color', 'dicm-divi-custom-modules' ),
				'type'            => 'color',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as title.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_05',
			),
			'image_05' => array(
				'label'           => esc_html__( 'Image', 'dicm-divi-custom-modules' ),
				'type'            => 'upload',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as subtitle.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'grid_05',
			),
		);
	}

	// Rendering into browser
	public function render( $attrs, $content = null, $render_slug ) {
		$button_text           = $this->props['button_text'];
		$button_url            = $this->props['button_url'];
		$button_url_new_window = $this->props['button_url_new_window'];

		ob_start();
		?>
		<div class="wrapper <?php echo $this->new_dom($this->props['hero_type'], 'no-home'); ?>">
			<div class="box1">
				<?php echo $this->new_dom($this->props['hero_type'], '<div class="box1-inner">'); ?>
					<h1><?php echo $this->props['title']; ?></h1>
					<?php echo apply_filters('the_content', $this->content); ?>
					<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_left et_pb_module"
						style="margin-top: 40px;">
						<a href="<?php echo esc_url($button_url); ?>"
							class="btnReverse et_pb_button et_pb_button_0 btnInline et_pb_bg_layout_light"
							style="<?php echo $this->button_style( $this->props['hero_type'] ); ?>"
							target="<?php echo $button_url_new_window; ?>">
							<?php echo $button_text; ?>
						</a>
					</div>
				<?php echo $this->new_dom($this->props['hero_type'], '</div>'); ?>
			</div>

			<?php $this->grids(); ?>
		</div>

		<?php if( $this->props['hero_type'] == 'on' ) : ?>
		<div class="wrapper2">
          <div class="box6">
          </div>
          <div class="box7">
          </div>
          <div class="box8">
          </div>
          <div class="box9">
          </div>
          <div class="box10">
          </div>
        </div>
		<?php
		endif;
		return ob_get_clean();
	}

}

new PyxHero;
