<?php
/*
Plugin Name: Pyx Assistant
Plugin URI:
Description: An extension to extend Pyxhealth website's features and functionalities.
Version:     1.1.0
Author:      TreeTreeAgency
Author URI:  https://treetreeagency.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: pyx-pyx-assistant
Domain Path: /languages

Pyx Assistant is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Pyx Assistant is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pyx Assistant. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/


if ( ! function_exists( 'pyx_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function pyx_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/PyxAssistant.php';
}
add_action( 'divi_extensions_init', 'pyx_initialize_extension' );
endif;
